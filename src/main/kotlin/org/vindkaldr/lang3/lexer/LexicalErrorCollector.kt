package org.vindkaldr.lang3.lexer

interface LexicalErrorCollector {
    fun collectLexicalError(line: Int, position: Int)
}
