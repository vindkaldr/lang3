package org.vindkaldr.lang3.lexer.api

interface TokenCollector {
    fun collectEndOfFileToken(line: Int, position: Int)
    fun collectPrintLineToken(line: Int, position: Int)
    fun collectStringToken(value: String, line: Int, position: Int)
}
