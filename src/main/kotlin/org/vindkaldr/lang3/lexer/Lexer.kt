package org.vindkaldr.lang3.lexer

import org.vindkaldr.lang3.lexer.api.TokenCollector

private val spacesOrTabsPattern = Regex("^[ \t]+")
private val newLinesPattern = Regex("^\n+")
private val printLinePattern = Regex("^println")
private val stringPattern = Regex("^\".*?\"")

class Lexer(
    private val tokenCollector: TokenCollector,
    private val errorCollector: LexicalErrorCollector
) {
    private lateinit var remainingSource: String

    private var line = 1
    private var position = 1

    private var errorLine = 0
    private var errorPosition = 0

    fun lex(source: String) {
        remainingSource = source

        while (remainingSource.isNotEmpty()) {
            when {
                isWhitespaceCharacterSkipped() -> {}
                isTokenCollected() -> collectMarkedError()
                else -> markError()
            }
        }
        collectMarkedError()
        tokenCollector.collectEndOfFileToken(line, position)
    }

    private fun isWhitespaceCharacterSkipped(): Boolean {
        return skipSpacesOrTabs() || skipNewLines()
    }

    private fun skipSpacesOrTabs(): Boolean {
        val value = spacesOrTabsPattern.find(remainingSource)?.value
        if (value != null) {
            position += value.length
            remainingSource = remainingSource.substring(value.length)
            return true
        }
        return false
    }

    private fun skipNewLines(): Boolean {
        val value = newLinesPattern.find(remainingSource)?.value
        if (value != null) {
            line += value.length
            position = 1
            remainingSource = remainingSource.substring(value.length)
            return true
        }
        return false
    }

    private fun isTokenCollected(): Boolean {
        return collectPrintLineToken() || collectStringToken()
    }

    private fun collectPrintLineToken(): Boolean {
        val value = printLinePattern.find(remainingSource)?.value
        if (value != null) {
            tokenCollector.collectPrintLineToken(line, position)
            position += value.length
            remainingSource = remainingSource.substring(value.length)
            return true
        }
        return false
    }

    private fun collectStringToken(): Boolean {
        val value = stringPattern.find(remainingSource)?.value
        if (value != null) {
            tokenCollector.collectStringToken(value.trim('"'), line, position)
            position += value.length
            remainingSource = remainingSource.substring(value.length)
            return true
        }
        return false
    }

    private fun collectMarkedError() {
        if (errorLine != 0 && errorPosition != 0) {
            errorCollector.collectLexicalError(errorLine, errorPosition)
            resetErrorPosition()
        }
    }

    private fun resetErrorPosition() {
        errorLine = 0
        errorPosition = 0
    }

    private fun markError() {
        markErrorPosition()
        advancePosition()
    }

    private fun markErrorPosition() {
        if (errorLine == 0 && errorPosition == 0) {
            errorLine = line
            errorPosition = position
        }
    }

    private fun advancePosition() {
        position++
        remainingSource = remainingSource.substring(1)
    }
}
