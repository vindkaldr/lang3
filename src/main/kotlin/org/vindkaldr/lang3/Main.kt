package org.vindkaldr.lang3

import org.vindkaldr.lang3.generator.ByteCodeGenerator
import org.vindkaldr.lang3.lexer.Lexer
import org.vindkaldr.lang3.parser.Parser
import org.vindkaldr.lang3.syntax.DefaultSyntaxBuilder

fun main(args: Array<String>) {
    val source = "println \"Hello World!\""

    val syntaxBuilder = DefaultSyntaxBuilder()
    val errorCollector = ErrorCollector()
    val parser = Parser(syntaxBuilder, errorCollector)
    val lexer = Lexer(parser, errorCollector)
    lexer.lex(source)

    if (!errorCollector.isErrorOccurred) {
        val codeGenerator = ByteCodeGenerator()
        codeGenerator.generate(syntaxBuilder.syntax)
    }
}
