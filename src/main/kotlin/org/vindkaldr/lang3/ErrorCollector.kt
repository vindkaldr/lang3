package org.vindkaldr.lang3

import org.vindkaldr.lang3.lexer.LexicalErrorCollector
import org.vindkaldr.lang3.parser.SyntacticErrorCollector

class ErrorCollector : LexicalErrorCollector, SyntacticErrorCollector {
    private var _isErrorOccurred = false

    val isErrorOccurred: Boolean
        get() = _isErrorOccurred

    override fun collectLexicalError(line: Int, position: Int) {
        _isErrorOccurred = true
        println("Lexical error in line $line at position $position")
    }

    override fun collectSyntacticError(line: Int, position: Int, errorMessage: String) {
        _isErrorOccurred = true
        println("Syntactic error in line $line at position $position: $errorMessage")
    }

}
