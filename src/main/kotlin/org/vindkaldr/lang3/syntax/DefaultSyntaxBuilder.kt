package org.vindkaldr.lang3.syntax

import org.vindkaldr.lang3.parser.api.SyntaxBuilder

class DefaultSyntaxBuilder : SyntaxBuilder {
    private lateinit var _syntax: Syntax

    val syntax : Syntax
        get() = _syntax

    override fun addPrintLineStatement(value: String) {
        _syntax = Syntax(PrintLineStatement(value))
    }
}
