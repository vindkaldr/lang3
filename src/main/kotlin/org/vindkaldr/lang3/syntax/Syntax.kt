package org.vindkaldr.lang3.syntax

data class Syntax(val printLineStatement: PrintLineStatement)
data class PrintLineStatement(val value: String)
