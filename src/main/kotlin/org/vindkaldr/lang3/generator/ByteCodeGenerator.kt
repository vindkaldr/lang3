package org.vindkaldr.lang3.generator

import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes.ACC_PUBLIC
import org.objectweb.asm.Opcodes.ACC_STATIC
import org.objectweb.asm.Opcodes.GETSTATIC
import org.objectweb.asm.Opcodes.INVOKEVIRTUAL
import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.Opcodes.V1_8
import org.vindkaldr.lang3.syntax.Syntax
import java.io.FileOutputStream

class ByteCodeGenerator {
    fun generate(syntax: Syntax) {
        val classWriter = ClassWriter(0)

        classWriter.visit(V1_8, ACC_PUBLIC, "Program", null, "java/lang/Object", emptyArray())

//        val constructor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null)
//        constructor.visitVarInsn(ALOAD, 0)
//        constructor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false)
//        constructor.visitMaxs(1, 1)
//        constructor.visitInsn(RETURN)

        val main = classWriter.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null)
        main.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;")
        main.visitLdcInsn(syntax.printLineStatement.value)
        main.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false)
        main.visitMaxs(2, 1)
        main.visitInsn(RETURN)

        val content = classWriter.toByteArray()
        FileOutputStream("./Program.class").write(content)
    }
}
