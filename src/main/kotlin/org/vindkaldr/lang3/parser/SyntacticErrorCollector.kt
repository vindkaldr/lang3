package org.vindkaldr.lang3.parser

interface SyntacticErrorCollector {
    fun collectSyntacticError(line: Int, position: Int, errorMessage: String)
}
