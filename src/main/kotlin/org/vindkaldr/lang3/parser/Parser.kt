package org.vindkaldr.lang3.parser

import org.vindkaldr.lang3.lexer.api.TokenCollector
import org.vindkaldr.lang3.parser.api.SyntaxBuilder

/**
 * <program> ::= <println-statement>
 * <println-statement> ::= "println" <string-literal>
 */

private val PARSER_TRANSITIONS = listOf(
    Transition(State.BEGIN, Event.PRINT_LINE, State.PRINT_LINE),
    Transition(State.PRINT_LINE, Event.STRING, State.STRING),
    Transition(State.STRING, Event.END_OF_FILE, State.END)
)

private class Transition(
    val state: State,
    val event: Event,
    val nextState: State
)

private enum class State {
    BEGIN, PRINT_LINE, STRING, END
}

private enum class Event {
    PRINT_LINE, STRING, END_OF_FILE
}

class Parser(
    private val syntaxBuilder: SyntaxBuilder,
    private val errorCollector: SyntacticErrorCollector
) : TokenCollector {
    private var currentState = State.BEGIN

    override fun collectEndOfFileToken(line: Int, position: Int) {
        handleEvent(Event.END_OF_FILE, line, position)
    }

    override fun collectPrintLineToken(line: Int, position: Int) {
        handleEvent(Event.PRINT_LINE, line, position)
    }

    override fun collectStringToken(value: String, line: Int, position: Int) {
        syntaxBuilder.addPrintLineStatement(value)
        handleEvent(Event.STRING, line, position)
    }

    private fun handleEvent(event: Event, line: Int, position: Int) {
        val transition = getTransition(event)
        if (transition != null) {
            handleTransition(transition)
        }
        else {
            handleError(line, position)
        }
    }

    private fun getTransition(event: Event): Transition? {
        return PARSER_TRANSITIONS.firstOrNull { it.state == currentState && it.event == event }
    }

    private fun handleTransition(transition: Transition) {
        currentState = transition.nextState
    }

    private fun handleError(line: Int, position: Int) {
        when(currentState) {
            State.BEGIN -> errorCollector.collectSyntacticError(line, position, "Expected println statement!")
            State.PRINT_LINE -> errorCollector.collectSyntacticError(line, position, "Expected string literal!")
            State.STRING -> errorCollector.collectSyntacticError(line, position, "Expected end of file!")
            State.END -> errorCollector.collectSyntacticError(line, position, "Unexpected statement/literal!")
        }
    }
}
