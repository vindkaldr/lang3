package org.vindkaldr.lang3.parser.api

interface SyntaxBuilder {
    fun addPrintLineStatement(value: String)
}
