package org.vindkaldr.lang3

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.junit.Test
import org.vindkaldr.lang3.generator.ByteCodeGenerator
import org.vindkaldr.lang3.lexer.Lexer
import org.vindkaldr.lang3.parser.Parser
import org.vindkaldr.lang3.syntax.DefaultSyntaxBuilder

class Lang3Test {
    @Test
    fun `compiles println statement`() {
        val source = "println \"Hello World!\""

        val syntaxBuilder = DefaultSyntaxBuilder()
        val errorCollector = ErrorCollector()
        val parser = Parser(syntaxBuilder, errorCollector)
        val lexer = Lexer(parser, errorCollector)
        val codeGenerator = ByteCodeGenerator()
        lexer.lex(source)
        codeGenerator.generate(syntaxBuilder.syntax)

        val builder = ProcessBuilder("java", "Program")
        builder.redirectErrorStream(true)
        val process = builder.start()

        assert.that(process.inputStream.bufferedReader().readText(), equalTo("Hello World!\n"))
    }
}
