package org.vindkaldr.lang3.generator

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.junit.Test
import org.vindkaldr.lang3.syntax.PrintLineStatement
import org.vindkaldr.lang3.syntax.Syntax

class ByteCodeGeneratorTest {
    @Test
    fun `generates println statement`() {
        val codeGenerator = ByteCodeGenerator()
        codeGenerator.generate(Syntax(PrintLineStatement("Hello World!")))

        val builder = ProcessBuilder("java", "Program")
        builder.redirectErrorStream(true)
        val process = builder.start()

        assert.that(process.inputStream.bufferedReader().readText(), equalTo("Hello World!\n"))
    }
}
