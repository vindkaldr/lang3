package org.vindkaldr.lang3.syntax

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.junit.Test

class DefaultSyntaxBuilderTest {
    @Test
    fun `builds println statement`() {
        val syntaxBuilder = DefaultSyntaxBuilder()

        syntaxBuilder.addPrintLineStatement("Hello World!")

        assert.that(syntaxBuilder.syntax, equalTo(Syntax(PrintLineStatement("Hello World!"))))
    }
}
