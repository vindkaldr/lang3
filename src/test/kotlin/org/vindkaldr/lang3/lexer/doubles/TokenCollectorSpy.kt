package org.vindkaldr.lang3.lexer.doubles

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.vindkaldr.lang3.lexer.Lexer
import org.vindkaldr.lang3.lexer.api.TokenCollector

fun assertTokens(source: String, expectedTokens: String) {
    val tokenCollector = TokenCollectorSpy()
    Lexer(tokenCollector, LexicalErrorCollectorSpy()).lex(source)
    assert.that(tokenCollector.capturedTokens, equalTo(expectedTokens))
}

open class TokenCollectorSpy : TokenCollector {
    private var _capturedTokens = ""

    val capturedTokens: String
        get() = _capturedTokens

    override fun collectEndOfFileToken(line: Int, position: Int) {
        addToken("eof")
    }

    override fun collectPrintLineToken(line: Int, position: Int) {
        addToken("println")
    }

    override fun collectStringToken(value: String, line: Int, position: Int) {
        addToken("string")
    }

    protected fun addToken(token: String) {
        _capturedTokens += if (_capturedTokens.isBlank()) token else ",$token"
    }
}
