package org.vindkaldr.lang3.lexer.doubles

import com.natpryce.hamkrest.equalTo
import org.vindkaldr.lang3.lexer.LexicalErrorCollector
import com.natpryce.hamkrest.assertion.assert
import org.vindkaldr.lang3.lexer.Lexer

fun assertErrors(source: String, expectedTokens: String, expectedErrors: String) {
    val tokenCollector = TokenCollectorSpy()
    val errorCollector = LexicalErrorCollectorSpy()
    Lexer(tokenCollector, errorCollector).lex(source)
    assert.that(tokenCollector.capturedTokens, equalTo(expectedTokens))
    assert.that(errorCollector.capturedErrors, equalTo(expectedErrors))
}

open class LexicalErrorCollectorSpy : LexicalErrorCollector {
    private var _capturedErrors = ""

    val capturedErrors: String
        get() = _capturedErrors

    override fun collectLexicalError(line: Int, position: Int) {
        addError("error")
    }

    protected fun addError(error: String) {
        _capturedErrors += if (_capturedErrors.isBlank()) error else ",$error"
    }
}
