package org.vindkaldr.lang3.lexer.doubles

import com.natpryce.hamkrest.equalTo
import org.vindkaldr.lang3.lexer.Lexer

fun assertErrorsWithPosition(source: String, expectedTokens: String, expectedErrors: String){
    val tokenCollector = TokenCollectorSpy()
    val errorCollector = LexicalErrorPositionCollectorSpy()
    Lexer(tokenCollector, errorCollector).lex(source)
    com.natpryce.hamkrest.assertion.assert.that(tokenCollector.capturedTokens, equalTo(expectedTokens))
    com.natpryce.hamkrest.assertion.assert.that(errorCollector.capturedErrors, equalTo(expectedErrors))
}

private class LexicalErrorPositionCollectorSpy : LexicalErrorCollectorSpy() {
    override fun collectLexicalError(line: Int, position: Int) {
        addError("error:$line.$position")
    }
}
