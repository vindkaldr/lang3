package org.vindkaldr.lang3.lexer

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.junit.Test
import org.vindkaldr.lang3.lexer.doubles.LexicalErrorCollectorSpy
import org.vindkaldr.lang3.lexer.doubles.TokenCollectorSpy
import org.vindkaldr.lang3.lexer.doubles.assertErrors
import org.vindkaldr.lang3.lexer.doubles.assertErrorsWithPosition
import org.vindkaldr.lang3.lexer.doubles.assertTokens

class StringLiteralLexerTest {
    @Test
    fun `collects token for empty string`() {
        assertTokens("\"\"", "string,eof")
    }

    @Test
    fun `collects token for string`() {
        assertTokens("\"Hello World!\"", "string,eof")
        assertTokens(" \"Hello World!\" ", "string,eof")
        assertTokens("\t\"Hello World!\"\t", "string,eof")
        assertTokens("\n\"Hello World!\"\n", "string,eof")
    }

    @Test
    fun `collects tokens for strings`() {
        assertTokens("\"Hello\"\"World!\"", "string,string,eof")
        assertTokens(" \"Hello\" \"World!\" ", "string,string,eof")
        assertTokens("\t\"Hello\"\t\"World!\"\t", "string,string,eof")
        assertTokens("\n\"Hello\"\n\"World!\"\n", "string,string,eof")
    }

    @Test
    fun `collects value for string`() {
        assertTokensWithValue("\"Hello World!\"", "string:Hello World!,eof")
    }

    @Test
    fun `collects position for string`() {
        assertTokensWithPosition("\"Hello World!\"", "string:1.1,eof")
        assertTokensWithPosition(" \"Hello World!\"", "string:1.2,eof")
        assertTokensWithPosition("   \"Hello World!\"", "string:1.4,eof")
        assertTokensWithPosition("\t\"Hello World!\"", "string:1.2,eof")
        assertTokensWithPosition("\t\t\t\"Hello World!\"", "string:1.4,eof")
        assertTokensWithPosition("\n\"Hello World!\"", "string:2.1,eof")
        assertTokensWithPosition("\n\n\n\"Hello World!\"", "string:4.1,eof")
        assertTokensWithPosition("\n \"Hello World!\"", "string:2.2,eof")
    }

    @Test
    fun `collects error for invalid string`() {
        assertErrors("\"", "eof", "error")
        assertErrors("\"Hello", "eof", "error")
        assertErrors("Hello\"", "eof", "error")
        assertErrors("\"Hello World!", "eof", "error")
        assertErrors("Hello World!\"", "eof", "error")
        assertErrors("\"Hello\"World!", "string,eof", "error")
        assertErrors("Hello\"World!\"", "string,eof", "error")
        assertErrors("\"Hello\"\"World!", "string,eof", "error")
        assertErrors("\"Hello\"World!\"", "string,eof", "error")
        assertErrors("\n \"Hello\"World!\"", "string,eof", "error")
    }

    @Test
    fun `collects errors for invalid strings`() {
        assertErrors("Hello\"\"World!", "string,eof", "error,error")
        assertErrors("Hello\"\"World!\"", "string,eof", "error,error")
        assertErrors("\n Hello\"\"World!\"", "string,eof", "error,error")
    }

    @Test
    fun `collects position for invalid string`() {
        assertErrorsWithPosition("\"", "eof", "error:1.1")
        assertErrorsWithPosition("\"Hello", "eof", "error:1.1")
        assertErrorsWithPosition("Hello\"", "eof", "error:1.1")
        assertErrorsWithPosition("\"Hello World!", "eof", "error:1.1")
        assertErrorsWithPosition("Hello World!\"", "eof", "error:1.1")
        assertErrorsWithPosition("\"Hello\"World!", "string,eof", "error:1.8")
        assertErrorsWithPosition("Hello\"World!\"", "string,eof", "error:1.1")
        assertErrorsWithPosition("\"Hello\"\"World!", "string,eof", "error:1.8")
        assertErrorsWithPosition("\"Hello\"World!\"", "string,eof", "error:1.8")
        assertErrorsWithPosition("\n \"Hello\"World!\"", "string,eof", "error:2.9")
    }

    @Test
    fun `collects positions for invalid strings`() {
        assertErrorsWithPosition("Hello\"\"World!", "string,eof", "error:1.1,error:1.8")
        assertErrorsWithPosition("Hello\"\"World!\"", "string,eof", "error:1.1,error:1.8")
        assertErrorsWithPosition("\n Hello\"\"World!\"", "string,eof", "error:2.2,error:2.9")
    }
}

private fun assertTokensWithValue(source: String, expectedTokens: String) {
    val tokenCollector = StringValueSpy()
    Lexer(tokenCollector, LexicalErrorCollectorSpy()).lex(source)
    assert.that(tokenCollector.capturedTokens, equalTo(expectedTokens))
}

private class StringValueSpy : TokenCollectorSpy() {
    override fun collectStringToken(value: String, line: Int, position: Int) {
        addToken("string:$value")
    }
}

private fun assertTokensWithPosition(source: String, expectedTokens: String) {
    val tokenCollector = StringPositionSpy()
    Lexer(tokenCollector, LexicalErrorCollectorSpy()).lex(source)
    assert.that(tokenCollector.capturedTokens, equalTo(expectedTokens))
}

private class StringPositionSpy : TokenCollectorSpy() {
    override fun collectStringToken(value: String, line: Int, position: Int) {
        addToken("string:$line.$position")
    }
}
