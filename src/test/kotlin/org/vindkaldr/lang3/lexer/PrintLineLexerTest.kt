package org.vindkaldr.lang3.lexer

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.junit.Test
import org.vindkaldr.lang3.lexer.doubles.LexicalErrorCollectorSpy
import org.vindkaldr.lang3.lexer.doubles.TokenCollectorSpy
import org.vindkaldr.lang3.lexer.doubles.assertErrors
import org.vindkaldr.lang3.lexer.doubles.assertErrorsWithPosition
import org.vindkaldr.lang3.lexer.doubles.assertTokens

class PrintLineStatementLexerTest {
    @Test
    fun `collects token for println`() {
        assertTokens("println", "println,eof")
        assertTokens(" println ", "println,eof")
        assertTokens("\tprintln\t", "println,eof")
        assertTokens("\nprintln\n", "println,eof")
    }

    @Test
    fun `collects position for println`() {
        assertTokensWithPosition("println", "println:1.1,eof")
        assertTokensWithPosition(" println", "println:1.2,eof")
        assertTokensWithPosition("   println", "println:1.4,eof")
        assertTokensWithPosition("\tprintln", "println:1.2,eof")
        assertTokensWithPosition("\t\t\tprintln", "println:1.4,eof")
        assertTokensWithPosition("\nprintln", "println:2.1,eof")
        assertTokensWithPosition("\n\n\nprintln", "println:4.1,eof")
        assertTokensWithPosition("\n println", "println:2.2,eof")
    }

    @Test
    fun `collects error for println`() {
        assertErrors("print", "eof", "error")
        assertErrors(" print", "eof", "error")
        assertErrors("\tprint", "eof", "error")
        assertErrors("\nprint", "eof", "error")
    }

    @Test
    fun `collects error position for println`() {
        assertErrorsWithPosition("print", "eof", "error:1.1")
        assertErrorsWithPosition(" print", "eof", "error:1.2")
        assertErrorsWithPosition("   print", "eof", "error:1.4")
        assertErrorsWithPosition("\tprint", "eof", "error:1.2")
        assertErrorsWithPosition("\t\t\tprint", "eof", "error:1.4")
        assertErrorsWithPosition("\nprint", "eof", "error:2.1")
        assertErrorsWithPosition("\n\n\nprint", "eof", "error:4.1")
        assertErrorsWithPosition("\n print", "eof", "error:2.2")
    }

    @Test
    fun `collects tokens for println and string`() {
        assertTokens("println\"Hello World!\"", "println,string,eof")
        assertTokens("println \"Hello World!\" ", "println,string,eof")
        assertTokens("println\t\"Hello World!\"\t", "println,string,eof")
        assertTokens("println\n\"Hello World!\"\n", "println,string,eof")
    }
}

private fun assertTokensWithPosition(source: String, expectedTokens: String) {
    val tokenCollector = PrintLinePositionSpy()
    Lexer(tokenCollector, LexicalErrorCollectorSpy()).lex(source)
    assert.that(tokenCollector.capturedTokens, equalTo(expectedTokens))
}

private class PrintLinePositionSpy : TokenCollectorSpy() {
    override fun collectPrintLineToken(line: Int, position: Int) {
        addToken("println:$line.$position")
    }
}
