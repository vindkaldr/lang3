package org.vindkaldr.lang3.lexer

import com.natpryce.hamkrest.equalTo
import org.junit.Test
import org.vindkaldr.lang3.lexer.doubles.LexicalErrorCollectorSpy
import org.vindkaldr.lang3.lexer.doubles.TokenCollectorSpy
import org.vindkaldr.lang3.lexer.doubles.assertTokens

class WhitespaceCharacterLexerTest {
    @Test
    fun `collects token for empty source`() {
        assertTokens("", "eof")
    }

    @Test
    fun `collects token for blank source`() {
        assertTokens(" ", "eof")
        assertTokens("   ", "eof")
        assertTokens("\t", "eof")
        assertTokens("\t\t\t", "eof")
        assertTokens("\n", "eof")
        assertTokens("\n\n\n", "eof")
    }

    @Test
    fun `collects position for empty or blank source`() {
        assertTokensWithPosition("", "eof:1.1")
        assertTokensWithPosition(" ", "eof:1.2")
        assertTokensWithPosition("   ", "eof:1.4")
        assertTokensWithPosition("\t", "eof:1.2")
        assertTokensWithPosition("\t\t\t", "eof:1.4")
        assertTokensWithPosition("\n", "eof:2.1")
        assertTokensWithPosition("\n\n\n", "eof:4.1")
        assertTokensWithPosition("\n ", "eof:2.2")
    }
}

private fun assertTokensWithPosition(source: String, expectedTokens: String) {
    val tokenCollector = EndOfFilePositionSpy()
    Lexer(tokenCollector, LexicalErrorCollectorSpy()).lex(source)
    com.natpryce.hamkrest.assertion.assert.that(tokenCollector.capturedTokens, equalTo(expectedTokens))
}

private class EndOfFilePositionSpy : TokenCollectorSpy() {
    override fun collectEndOfFileToken(line: Int, position: Int) {
        addToken("eof:$line.$position")
    }
}
