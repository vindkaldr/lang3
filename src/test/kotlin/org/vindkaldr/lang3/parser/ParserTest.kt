package org.vindkaldr.lang3.parser

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.junit.Before
import org.junit.Test
import org.vindkaldr.lang3.parser.api.SyntaxBuilder

class ParserTest {
    private lateinit var syntaxBuilder: SyntaxBuilderSpy
    private lateinit var errorCollector: SyntacticErrorCollectorSpy
    private lateinit var parser: Parser

    @Before
    fun setUp() {
        syntaxBuilder = SyntaxBuilderSpy()
        errorCollector = SyntacticErrorCollectorSpy()
        parser = Parser(syntaxBuilder, errorCollector)
    }

    @Test
    fun `collects error when expected println`() {
        parser.collectEndOfFileToken(0, 0)

        assert.that(syntaxBuilder.capturedSyntax, equalTo(""))
        assert.that(errorCollector.captureErrors, equalTo("0.0.Expected println statement!"))
    }

    @Test
    fun `collects error when expected string`() {
        parser.collectPrintLineToken(0, 0)
        parser.collectEndOfFileToken(0, 1)

        assert.that(syntaxBuilder.capturedSyntax, equalTo(""))
        assert.that(errorCollector.captureErrors, equalTo("0.1.Expected string literal!"))
    }

    @Test
    fun `collects error when expected end of file`() {
        parser.collectPrintLineToken(0, 0)
        parser.collectStringToken("Hello World!", 0, 1)
        parser.collectPrintLineToken(0, 2)

        assert.that(syntaxBuilder.capturedSyntax, equalTo("println \"Hello World!\""))
        assert.that(errorCollector.captureErrors, equalTo("0.2.Expected end of file!"))
    }

    @Test
    fun `builds println statement`() {
        parser.collectPrintLineToken(0, 0)
        parser.collectStringToken("Hello World!", 0, 0)
        parser.collectEndOfFileToken(0, 0)

        assert.that(syntaxBuilder.capturedSyntax, equalTo("println \"Hello World!\""))
        assert.that(errorCollector.captureErrors, equalTo(""))
    }
}

private class SyntaxBuilderSpy : SyntaxBuilder {
    private var _capturedSyntax = ""

    val capturedSyntax: String
        get() = _capturedSyntax

    override fun addPrintLineStatement(value: String) {
        _capturedSyntax = "println \"$value\""
    }
}

private class SyntacticErrorCollectorSpy : SyntacticErrorCollector {
    private var _capturedErrors = ""

    val captureErrors: String
        get() = _capturedErrors

    override fun collectSyntacticError(line: Int, position: Int, errorMessage: String) {
        _capturedErrors = "$line.$position.$errorMessage"
    }
}
